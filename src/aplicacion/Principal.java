package aplicacion;
import dominio.*;

public class Principal{
    public static void main (String[] args){

        Localidad vallecas = new Localidad();
        vallecas.setNumeroDeHabitantes(40000);
        Localidad tetuan = new Localidad();
      	tetuan.setNumeroDeHabitantes(35000);
        Localidad alcobendas = new Localidad();
        alcobendas.setNumeroDeHabitantes(30000);

        Localidad torreblanca = new Localidad();
        torreblanca.setNumeroDeHabitantes(25000);
        Localidad elencinar = new Localidad();
        elencinar.setNumeroDeHabitantes(12000);
        Localidad navacerrada = new Localidad();
        navacerrada.setNumeroDeHabitantes(16000);

        Municipio madrid = new Municipio();
        madrid.setNombre ("Madrid ");

        Municipio leganes = new Municipio();
        leganes.setNombre ("Leganes ");



        madrid.coleccionLocalidades.add(vallecas);
        madrid.coleccionLocalidades.add(tetuan);
        madrid.coleccionLocalidades.add(alcobendas);

        madrid.calcularNumeroHabitantes();
        System.out.println(madrid);


	leganes.coleccionLocalidades.add(torreblanca);
        leganes.coleccionLocalidades.add(elencinar);
        leganes.coleccionLocalidades.add(navacerrada);


        leganes.calcularNumeroHabitantes();
        System.out.println(leganes);

        Provincia provincia = new Provincia();
        provincia.setNombre ("Comunidad de Madrid ");



        provincia.coleccionMunicipios.add(madrid);
        provincia.coleccionMunicipios.add(leganes);

        provincia.calcularNumeroHabitantes();
        System.out.println(provincia);

    }
}

